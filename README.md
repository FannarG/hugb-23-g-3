Welcome to Clap-IT. 

Follow these steps to get started with Clap-IT:

**Step 1: Clone the Repository:**
`https://gitlab.com/FannarG/hugb-23-g-3.git`

**Step 2: Install Dependencies:**
- Install gRPC: 
`pip install grpcio`
- Install gRPC tools: 
`pip install grpcio-tools`
- Install protobuf 3.20.0:
`pip install protobuf==3.20.0`
- Install Coverage(If you want to run tests with coverage reports): 
`pip install coverage`



**To run application:**
- In the terminal go to application root directory, then go to src folder, use the command:`python server.py`
- In another terminal go to application root directory, then go to src folder and use the command: `python client.py`


**To run tests with coverage:**`
- In the terminal go to application root directory, then go to src folder and use the command: 
`coverage run -m unittest discover -s tests`
- Then type in your terminal: `coverage report` or `coverage html`



**Important:**


- Every time you make modifications to proto file you need to run the protoc. In the terminal go to application root directory, then go to src folder and use the command:
`python -m grpc_tools.protoc -I ./proto/ --python_out=./ --grpc_python_out=./ ./proto/proto.proto`

- For weatherStation proto file : 
`python -m grpc_tools.protoc -I ./proto/ --python_out=./ --grpc_python_out=./ ./proto/weatherStation.proto`
