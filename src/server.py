import grpc
import proto_pb2
import proto_pb2_grpc
import csv
import weatherStation_pb2
import weatherStation_pb2_grpc
from concurrent import futures
from logic.user import Logic
from logic.bus_schedule import BusSchedule
from logic.bus import Bus
from logic.weather import Weather
from logic.favorites_bus_stops import FavoriteBusStops



class App(proto_pb2_grpc.AppServicer, weatherStation_pb2_grpc.WeatherStationServicer):
    """
    gRPC service implementation for the App service.
    This class handles gRPC service methods related to user management.
    """
    def __init__(self):
        """
        Initialize the AppServicer.
        This constructor creates an instance of the Logic class for user management.
        """
        self.logic = Logic()  # Assuming you have a Logic class for user management
        self.bus_schedule = BusSchedule(gtfs_folder = 'gtfs/')
        self.bus = Bus(gtfs_folder = 'gtfs/')
        

    def CreateUser(self, request, context):
        """
        gRPC service method for creating a new user.
        Args:
            request (CreateUserRequest): The request object containing user data.
            context: The context of the gRPC call.
        Returns:
            CreateUserResponse: A response object indicating the result of user creation.
        """
        name = request.name
        email = request.email
        password = request.password
        security_question = request.security_question

        user_id = self.logic.create_user(request)
    
        return proto_pb2.CreateUserResponse(message="User created successfully")


    def Login(self, request, context):
        """
        gRPC service method for user login.
        Args:
            request (LoginRequest): The request object containing user credentials.
            context: The context of the gRPC call.
        Returns:
            LoginResponse: A response object indicating the result of the login attempt.
        """

        email = request.email
        password = request.password
        success = self.logic.check_credentials(email, password)

        if success:
            self.logic.logged_in = True

        return proto_pb2.LoginResponse(success=success, message="Login successful" if success else "Login failed")

    def ChangePassword(self, request, context):
        """
        gRPC service method for changing a user's password.

        Args:
            request (ChangePasswordRequest): The request object containing user data.
            context: The context of the gRPC call.

        Returns:
            ChangePasswordResponse: A response object indicating the result of the password change.
        """
        email = request.email
        old_password = request.old_password
        new_password = request.new_password

        if self.change_password(email, old_password, new_password):
            return proto_pb2.ChangePasswordResponse(success=True, message="Password changed successfully")
        else:
            return proto_pb2.ChangePasswordResponse(success=False, message="Password change failed")
    

    def change_password(self, email, old_password, new_password):
        """
        Change the password for a user.

        Args:
            email (str): The user's email.
            old_password (str): The user's current password.
            new_password (str): The new password to set.

        Returns:
            bool: True if the password change was successful, False otherwise.
        """
        # Load user data from "Users.csv" file
        with open("Users.csv", "r") as user_file:
            user_csv = csv.reader(user_file)
            rows = list(user_csv)

        # Find the row with the matching email and old password
        for i, row in enumerate(rows):
            if row[2] == email and row[3] == old_password:
                if old_password == new_password:
                    return False  # Passwords are the same, no change needed
                # Update the password with the new password
                rows[i][3] = new_password
                # Write the updated data back to the file
                with open("Users.csv", "w", newline="") as user_file:
                    user_csv = csv.writer(user_file)
                    user_csv.writerows(rows)
                return True

        return False  # Password change failed (credentials not found)


    def ResetPassword(self, request, context):
        ''' Reset the user's password.'''
        email = request.email
        security_quest = request.security_quest
        new_password = request.new_password

        if self.Reset_helper(email, security_quest, new_password):
            return proto_pb2.ResetPasswordResponse(success=True, message="Password changed successfully")
        else:
            return proto_pb2.ResetPasswordResponse(success=False, message="Incorrect security question")

    def Reset_helper(self, email, security_quest, new_password):
        '''Helper function to reset the user's password.'''
        # Load user data from "Users.csv" file
        with open("Users.csv", "r") as user_file:
            user_csv = csv.reader(user_file)
            rows = list(user_csv)

        # Find the row with the matching email and old password
        for i, row in enumerate(rows):
            if row[2] == email and row[4] == security_quest:
                # Update the password with the new password
                rows[i][3] = new_password
                # Write the updated data back to the file
                with open("Users.csv", "w", newline="") as user_file:
                    user_csv = csv.writer(user_file)
                    user_csv.writerows(rows)
                return True

        return False  # Password change failed (credentials not found)






    def Change_Name(self, request, context):
        """
        gRPC service method for changing a user's name.

        Args:
            request (ChangeNameRequest): The request object containing user data.
            context: The context of the gRPC call.

        Returns:
            ChangeNameResponse: A response object indicating the result of the name change.
        """
        email = request.email
        new_name = request.new_name

        # Update the name for the specified email
        success = self.logic.update_name(email, new_name)

        if success:
            return proto_pb2.Change_NameResponse(success=True, message="Name updated successfully")
        else:
            return proto_pb2.Change_NameResponse(success=False, message="Failed to update name")


    def Change_Email(self, request, context):
        """
        gRPC service method for changing a user's email.
        Args:
            request (ChangeEmailRequest): The request object containing user data.
            context: The context of the gRPC call.
        Returns:
            ChangeEmailResponse: A response object indicating the result of the email change.
        """
        email = request.email
        new_name = request.new_email

        # Update the name for the specified email
        success = self.logic.update_email(email, new_name)

        if success:
            return proto_pb2.Change_EmailResponse(success=True, message="Email updated successfully")
        else:
            return proto_pb2.Change_EmailResponse(success=False, message="Failed to update Email")
        
    def GetBusSchedule(self, request, context):
        '''gRPC service method for getting bus schedules'''
       
        selected_route = request.selected_route
        selected_direction = request.selected_direction
        selected_day = request.selected_day

        schedule = self.bus_schedule.print_schedule(selected_route, selected_direction, selected_day)

        # Create and return a response message containing the schedule
        response = proto_pb2.BusScheduleResponse(schedule=schedule)

        return response
    

    def ViewArrivalTime(self, request, context):
        '''function for search by date and time option'''
        selected_start = request.selected_start
        selected_end = request.selected_end
        selected_time = request.selected_time
        
        arrival_time = self.bus.get_bus_times(selected_start, selected_end, selected_time)
        response = proto_pb2.ViewArrivalTime(arrival_time=arrival_time)

        return response
    def AddFavoriteBusStops(self, request, context):
        """takes the request and executes it and sends the response

        Args:
            request (str): string conataining and wives favorite bus stops
            context (str): _description_

        Returns:
            boolean: success = True 
        """
        fbs = FavoriteBusStops()
        email = request.email
        new_favorite_stops = list(request.new_favorite_stops)
        print(new_favorite_stops,email)
        print(type(new_favorite_stops))
        fbs.add_favorite(email, new_favorite_stops)
        print(f"Adding favorite bus stops for user {email}: {new_favorite_stops}")
        response = proto_pb2.AddFavoriteResponse(success= True)
        return response
    
    def RemoveFavoriteBusStops(self, request, context):
        """ takes the request and executes it and sends the response

        Args:
            request (str): string containing and removes favorite bus stops
            context (str): _description_

        Returns:
            boolen: success = True
        """
        fbs = FavoriteBusStops()
        email = request.email
        favorite_stops_to_remove = request.favorites_to_remove
        fbs.remove_favorites(email, favorite_stops_to_remove)
        print(f"Removing favorite bus stops for user {email}: {favorite_stops_to_remove}")
        response = proto_pb2.RemoveFavoriteResponse(success=True)
        return response
   
    def ChangeFavoriteBusStops(self, request, context):
        """ takes the request and executes it and sends the response

        Args:
            request (str): string containing and removes favorite bus stops
            context (str): _description_

        Returns:
            boolean: success = True
        """
        fbs = FavoriteBusStops()
        email = request.email
        new_favorites = request.new_favorites
        fbs.change_favorites(email, new_favorites)
        print(f"Changing favorite bus stops for user {email}: {new_favorites}")
        
        response = proto_pb2.ChangeFavoriteBusResponse(success=True)
        return response    
   
class WeatherStation(weatherStation_pb2_grpc.WeatherStationServicer):
    def get_current_weather(self, request, context):
        '''gRPC service method for retrieving current weather.'''
        
        SERVICE_URL = 'hugb-service-ep3thvejnq-uc.a.run.app:443'
        api_key = request.api_key
        latitude = request.lat
        longitude = request.lon

        # Make a gRPC call to the weather API
        with grpc.secure_channel(SERVICE_URL, grpc.ssl_channel_credentials()) as channel:

            stub = weatherStation_pb2_grpc.WeatherStationStub(channel)
            response = stub.get_current_weather(
                weatherStation_pb2.Location(
                    latitude=latitude,
                    longitude=longitude,
                    api_key=api_key
                )
            )

        # Extract data from the response and create a CurrentWeather message
        current_weather = weatherStation_pb2.CurrentWeather(
            temperature=response.temp,
            humidity=response.humidity,
            clouds=response.clouds,
            wind_speed=response.wind_speed,
            wind_direction=response.windDeg
        )

        return current_weather


       

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    proto_pb2_grpc.add_AppServicer_to_server(App(), server)
    weatherStation_pb2_grpc.add_WeatherStationServicer_to_server(WeatherStation(), server)
    server.add_insecure_port("[::]:50051")
    server.start()
    server.wait_for_termination()
    

if __name__ == "__main__":
    serve()
