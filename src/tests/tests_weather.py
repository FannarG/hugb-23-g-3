import os
import unittest
from unittest.mock import patch, mock_open
from logic.weather import Weather

class TestWeather(unittest.TestCase):

    def setUp(self):
        '''set up the instance'''
        self.weather = Weather("./gtfs")


    @patch('builtins.input', return_value='Hamraborg')
    def test_get_user_input_bus_stop(self, mock_input):
        '''Test the get_user_input_bus_stop method'''
        bus_stop_name = self.weather.get_user_input_bus_stop()
        self.assertEqual(bus_stop_name, 'Hamraborg')

    def test_read_gtfs_stops(self):
        ''' this test create a sample list of fake data to simulate stops'''
        fake_stops = [
            ('Stop1', '64.123', '-21.123'),
            ('Stop2', '64.456', '-21.456')
        ]

        # Set the fake stops data in the Weather instance
        self.weather.stops = fake_stops

        # Test your code that uses this fake data
        lat, lon = self.weather.get_stop_coordinates('Stop1')
        self.assertEqual(lat, '64.123')
        self.assertEqual(lon, '-21.123')

        lat, lon = self.weather.get_stop_coordinates('NonExistentStop')
        self.assertIsNone(lat)
        self.assertIsNone(lon)

    def test_get_stop_coordinates(self):
        '''Test the get_stop_coordinates method'''
        sample_stops = [('Stop1', '64.123', '-21.123'), ('Stop2', '64.456', '-21.456')]
        self.weather.stops = sample_stops

        lat, lon = self.weather.get_stop_coordinates('Stop1')
        self.assertEqual(lat, '64.123')
        self.assertEqual(lon, '-21.123')

        lat, lon = self.weather.get_stop_coordinates('NonExistentStop')
        self.assertIsNone(lat)
        self.assertIsNone(lon)

if __name__ == '__main__':
    unittest.main()
