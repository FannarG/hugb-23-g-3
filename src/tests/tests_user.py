import unittest
from unittest.mock import patch
from io import StringIO
from unittest.mock import patch, mock_open
from logic.user import Logic
import coverage
import csv

class TestLogic(unittest.TestCase):
    """
    A test class for the Logic class methods.
    """

    def setUp(self):
        """
        Set up the test environment by creating an instance of the Logic class.
        """
        self.logic = Logic()

    def tearDown(self):
        """
        Clean up the test environment after each test.
        """
        pass

    def test_check_credentials_valid(self):
        """
        Test the check_credentials method with valid credentials.
        """
        email = 'm'
        password = 'm'
        self.assertTrue(self.logic.check_credentials(email, password))

    def test_check_credentials_invalid(self):
        """
        Test the check_credentials method with invalid credentials.
        """
        email = 'nonexistent@example.com'
        password = 'wrongpassword'
        self.assertFalse(self.logic.check_credentials(email, password))


    def test_login_successful(self):
        """
        Test the login method with valid credentials.
        """
        email = "m"
        password = "m"
        self.assertTrue(self.logic.login(email, password))

    def test_login_failed(self):
        """
        Test the login method with invalid credentials.
        """
        email = "nonexistent@example.com"
        password = "password"
        self.assertFalse(self.logic.login(email, password))

    def test_email_exists_valid(self):
        """
        Test the email_exists method with an existing email.
        """
        email1 = 'magda@gmail.com'
        self.assertTrue(self.logic.email_exists(email1))

    def test_email_non_exists_valid(self):
        """
        Test the email_exists method with a non-existing email.
        """
        email2 = 'jondoe@chainmail.com'
        self.assertFalse(self.logic.email_exists(email2))

    def test_update_name_successful(self):
        """
        Test the update_name method for successful name update.
        """
        email = "john@example.com"
        new_name = "Jack"
        self.assertTrue(self.logic.update_name(email, new_name))

    def test_update_email_successful(self):
        """
        Test the update_email method for successful email update.
        """
        email = "john@example.com"
        new_email = "new_email@gmail.com"
        self.assertTrue(self.logic.update_email(email, new_email))

if __name__ == '__main__':
    unittest.main()

