import unittest
from unittest.mock import patch, mock_open
from logic.bus_schedule import BusSchedule
import os
from unittest import TestCase



class TestBusScheduleMethods(unittest.TestCase):
    @patch('logic.gtfs_setup.setup_gtfs_data')
    def test_create_route_id_to_short_name_mapping(self, mock_setup_gtfs_data):
        ''' Test creating a mapping of route IDs to short names.
        This test case checks whether the create_route_id_to_short_name_mapping method  correctly creates 
        a mapping of route IDs to short names based on predefined GTFS data.
        '''
        # Mock the setup function to return predefined data
        mock_setup_gtfs_data.return_value = (
            [],  # calendar_dates
            [  # List of routes
                ("AF.91", "1", "91", "Egilsstaðir <-> Norðfjörður", 3),
                ("AF.92", "1", "92", "Breiðdalsvík <-> Fáskrúðsfjörður", 3),
                ("AF.93", "1", "93", "Seyðisfjörður <-> Egilsstaðir", 3),
                ("AF.94", "1", "94", "Breiðdalsvík <-> Höfn", 3),
                ("AF.95", "1", "95", "Egilsstaðir <-> Borgarfjörður", 3)
            ],
            [],  # trips
            [],  # stop_times
            []   # stops
        )
        # Create a BusSchedule object
        bus_schedule = BusSchedule("dummy_gtfs_folder")  # The folder path doesn't matter for this test
        # Call the method with a list of routes
        route_id_to_short_name_mapping = bus_schedule.create_route_id_to_short_name_mapping([
            ("AF.91", "1", "91", "Egilsstaðir <-> Norðfjörður", 3),
            ("AF.92", "1", "92", "Breiðdalsvík <-> Fáskrúðsfjörður", 3),
            ("AF.93", "1", "93", "Seyðisfjörður <-> Egilsstaðir", 3),
            ("AF.94", "1", "94", "Breiðdalsvík <-> Höfn", 3),
            ("AF.95", "1", "95", "Egilsstaðir <-> Borgarfjörður", 3)
        ])
        # Define the expected mapping based on the provided data example
        expected_mapping = {
            "AF.91": "91",
            "AF.92": "92",
            "AF.93": "93",
            "AF.94": "94",
            "AF.95": "95"
        }
        # Assert that the actual mapping matches the expected mapping
        self.assertEqual(route_id_to_short_name_mapping, expected_mapping)

    @patch('builtins.input', return_value='1')
    @patch('logic.gtfs_setup.setup_gtfs_data', return_value=([], [], [], [], []))
    def test_get_selected_route(self, mock_input, mock_setup_gtfs_data):
        '''This test case verifies the functionality of the get_selected_route method 
        by mocking user input and GTFS data. It checks if the method correctly returns the selected route.
        '''
        # Import the BusSchedule class here
        bus_schedule = BusSchedule("dummy_gtfs_folder")
        selected_route = bus_schedule.get_selected_route()
        expected_result = '1'
        self.assertEqual(selected_route, expected_result)


    @patch('builtins.input', side_effect='1')
    @patch('logic.gtfs_setup.setup_gtfs_data', return_value=([], [], [], [], []))
    def test_get_selected_direction(self, mock_input, mock_setup_gtfs_data):
        '''This test case verifies the functionality of the get_selected_direction method 
        by mocking user input and GTFS data. 
        It also sets up mock data for bus_trip_headsigns and checks if the method returns the selected direction.'''
        # Mock the bus_trip_headsigns dictionary with options
        bus_schedule = BusSchedule("dummy_gtfs_folder")
        bus_schedule.bus_trip_headsigns = {'92': {'Direction 0', 'Direction 1'}}
        # Provide a route short name for which you have test data
        selected_direction = bus_schedule.get_selected_direction('92')
        expected_result = '1'
        self.assertEqual(selected_direction, expected_result)

    @patch('logic.gtfs_setup.setup_gtfs_data')
    def test_get_numeric_bus_numbers(self, mock_setup_gtfs_data):
        '''Test with a list of routes containing numeric and non-numeric route_short_names'''
        routes = [
            ("AF.91", "1", "91", "Egilsstaðir <-> Norðfjörður", 3),
            ("EF.95", "1", "95", "Egilsstaðir <-> Borgarfjörður", 3),
            ("BF.92", "1", "92", "Breiðdalsvík <-> Fáskrúðsfjörður", 3),
            ("DF.94", "1", "94x", "Breiðdalsvík <-> Höfn", 3),
        ]
        # Mock the setup_gtfs_data function to return dummy data
        mock_setup_gtfs_data.return_value = ([], routes, [], [], [])
        bus_schedule = BusSchedule("dummy_gtfs_folder")  # You can use a dummy folder for this test
        sorted_numeric_bus_numbers = bus_schedule.get_numeric_bus_numbers(routes)
        # Define the expected result
        expected_result = [91, 92, 95]
        # Assert that the actual result matches the expected result
        self.assertEqual(sorted_numeric_bus_numbers, expected_result)


    @patch('builtins.input', side_effect=['abc'])  # Provide an invalid route for testing
    @patch('logic.gtfs_setup.setup_gtfs_data', return_value=([], [], [], [], []))
    def test_validate_selected_route_invalid(self, mock_input, mock_setup_gtfs_data):
        ''' This test case checks the validate_selected_route method's functionality 
        by mocking user input and GTFS data with an invalid route. 
        It verifies that the method correctly returns False for an invalid route.
        '''
        bus_schedule = BusSchedule("dummy_gtfs_folder")
        # Call the method you want to test
        selected_route = bus_schedule.get_selected_route()
        # Call the function you want to test
        result = bus_schedule.validate_selected_route(selected_route)
        # Assert that the route is invalid
        self.assertFalse(result)


    @patch('builtins.input', return_value='20230101')
    @patch('logic.gtfs_setup.setup_gtfs_data', return_value=([], [], [], [], []))
    def test_get_selected_day(self, mock_input, mock_setup_gtfs_data):
        '''This test case verifies the functionality of the get_selected_day method 
        by mocking user input and GTFS data. 
        It checks if the method correctly returns the selected day.
        '''
        # Import the BusSchedule class here
        bus_schedule = BusSchedule("dummy_gtfs_folder")
        selected_day = bus_schedule.get_selected_day()
        expected_result = '20230101'
        self.assertEqual(selected_day, expected_result)



if __name__ == '__main__':
    unittest.main()

