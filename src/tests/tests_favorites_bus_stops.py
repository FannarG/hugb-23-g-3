import unittest
from unittest.mock import mock_open, patch
from io import StringIO
import csv 
from logic.favorites_bus_stops import FavoriteBusStops
import os

class TestFavoritesBusStops(unittest.TestCase):
    def setUp(self):
        '''creates a favorites stops instance'''
        self.favorite_stops_instance = FavoriteBusStops()
    '''
    def test_change_users_file(self):
        self.favorite_stops_instance.change_users_file()
        self.assertTrue(os.path.exists("Users.csv"))'''

    def test_add_favorite_function(self):
        '''reads favorites from users file and checks if particular favorite stops exist'''
        with open("Users.csv", "r") as file:
            reader = csv.reader(file)
            rows = list(reader)

        user_id_to_check = 2  # Replace with the user ID you're interested in
        user_row = [row for row in rows if row[0] == str(user_id_to_check)]  # Assuming user ID is at index 0
        favorite_stops = ['grandi', 'miklabraut']

        if user_row:
            user_favorites = user_row[0][1:]  # Favorites start from index 1

        # Check if any element in favorite_stops is present in the user's favorites
            found = any(favorite in user_favorites for favorite in favorite_stops)

            self.assertTrue(found, f"At least one favorite from {favorite_stops} not found in user {user_id_to_check}'s favorites.")
        else:
            self.fail(f"User ID {user_id_to_check} not found in the CSV.")

    def test_change_favorites(self):
        '''Changes the favorite bus stops for a user identified by their email.'''
        mock_file_content = "User ID,Name,Email,fbs-1,fbs-2,fbs-3,fbs-4\n1,John,Doe,,,,,\n2,Jane,Smith,Stop1,Stop2,,"
        with patch('builtins.open', mock_open(read_data=mock_file_content)):
            email = "jane@example.com"
            new_favorite_stops = ["NewStop1", "NewStop2", "NewStop3"]
            self.favorite_stops_instance.change_favorites(email, new_favorite_stops)
        
        # Add your assertions here to verify that the favorite stops were changed correctly
        
    def test_remove_favorites(self):
        """Test removing favorite bus stops for a user."""
        mock_file_content = "User ID,Name,Email,fbs-1,fbs-2,fbs-3,fbs-4\n1,John,Doe,,,,,\n2,Jane,Smith,Stop1,Stop2,,"
        with patch('builtins.open', mock_open(read_data=mock_file_content)):
            email = "jane@example.com"
            favorites_to_remove = ["Stop1", "Stop2"]
            self.favorite_stops_instance.remove_favorites(email, favorites_to_remove)

    def test_print_favorites(self):
        """Test printing favorite bus stops for a user."""
        email = "jane@example.com"
        favorites_to_find = ["Stop1"]
        file_content = "User ID,Name,Email,fbs-1,fbs-2,fbs-3,fbs-4\n2,Jane,Smith,Stop1,Stop2,,"
        
        with patch('builtins.open', mock_open(read_data=file_content)):
            with patch('sys.stdout', new_callable=StringIO) as mock_stdout:
                self.favorite_stops_instance.print_favorites(email, favorites_to_find)
      
        
 
        



if __name__ == '__main__':
    unittest.main()