import unittest
from logic.gtfs_setup import read_gtfs_file, parse_gtfs_data, setup_gtfs_data
from unittest.mock import mock_open, patch
import os
import tempfile

class TestGTFSDataSetup(unittest.TestCase):
    def test_parse_gtfs_data(self):
        '''This test case checks whether the parse_gtfs_data function correctly parses GTFS data.'''
        gtfs_data = ["1,Test Data", "2,Another Data"]
        parsed_data = parse_gtfs_data(gtfs_data)
        expected_parsed_data = [["1", "Test Data"], ["2", "Another Data"]]
        self.assertEqual(parsed_data, expected_parsed_data)


    def test_setup_gtfs_data(self):
        '''his test case verifies the functionality of the setup_gtfs_data function by creating a temporary directory, 
        populating it with mock GTFS files, and then calling the function to parse these files. 
        It checks if the parsed data matches the expected values for calendar_dates, routes, trips, stop_times, and stops.'''
        # Create a temporary directory for testing
        with tempfile.TemporaryDirectory() as temp_dir:
            # Create mock GTFS files
            mock_gtfs_data = "1,Data1,Value1\n2,Data2,Value2"
            with open(os.path.join(temp_dir, 'calendar_dates.txt'), 'w', encoding='utf-8') as f:
                f.write(mock_gtfs_data)
            with open(os.path.join(temp_dir, 'routes.txt'), 'w', encoding='utf-8') as f:
                f.write(mock_gtfs_data)
            with open(os.path.join(temp_dir, 'trips.txt'), 'w', encoding='utf-8') as f:
                f.write(mock_gtfs_data)
            with open(os.path.join(temp_dir, 'stop_times.txt'), 'w', encoding='utf-8') as f:
                f.write(mock_gtfs_data)
            with open(os.path.join(temp_dir, 'stops.txt'), 'w', encoding='utf-8') as f:
                f.write(mock_gtfs_data)

            # Call the setup_gtfs_data function
            calendar_dates, routes, trips, stop_times, stops = setup_gtfs_data(temp_dir)

            # Check if the data matches the expected values
            expected_data = [["1", "Data1", "Value1"], ["2", "Data2", "Value2"]]
            self.assertEqual(calendar_dates, expected_data)
            self.assertEqual(routes, expected_data)
            self.assertEqual(trips, expected_data)
            self.assertEqual(stop_times, expected_data)
            self.assertEqual(stops, expected_data)

if __name__ == '__main__':
    unittest.main()
