import unittest
from unittest.mock import patch
from io import StringIO
from unittest.mock import patch, mock_open
import coverage 
from logic.search_routes import SearchRoutes

class TestSearchRoutes(unittest.TestCase):
    
    
    def setUp(self):
        '''Set up the test environment by creating a SearchRoutes instance with the provided GTFS data folder'''
        self.search_routes = SearchRoutes("./gtfs")
        
    def test_create_route_short_name_to_long_name_mapping(self):
        '''Test the creation of route short name to long name mapping.'''
        expected_mapping = {
            '92': {"Breiðdalsvík <-> Fáskrúðsfjörður"},
            }
        actual_mapping = self.search_routes.route_short_name_to_long_name
        for route_short_name, expected_long_names in expected_mapping.items():
            self.assertIn(route_short_name, actual_mapping)
            self.assertEqual(actual_mapping[route_short_name],expected_long_names)
            
    def test_create_direction_set(self):
        '''Test the creation of a set of numeric bus numbers from the routes data.'''
        numeric_bus_numbers = self.search_routes.get_numeric_bus_numbers(self.search_routes.routes)

        # Define a set of known numeric bus numbers
        known_numeric_numbers = {4, 5, 6}  # Example set of known numeric bus numbers

        # Check if known numbers are present in the obtained numeric bus numbers
        for number in known_numeric_numbers:
            with self.subTest(number=number):
                self.assertIn(number, numeric_bus_numbers)