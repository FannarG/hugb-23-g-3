import unittest
import unittest.mock
import io
from unittest.mock import patch
from io import StringIO
from unittest.mock import patch, mock_open
import coverage
from logic.bus import Bus


class TestBus(unittest.TestCase):
    
    
    def setUp(self):
        # Create a Bus instance with test data
        self.bus = Bus("./gtfs")  # Provide a test GTFS folder path


    def test_compare_tripID(self):
        """test to compare trips id"""
        start_trip_ids = [123, 456, 789]
        end_trip_ids = [456, 789, 101]
        common_trip_ids = self.bus.compare_tripID(start_trip_ids, end_trip_ids)
        self.assertEqual(common_trip_ids, [456, 789])  # Replace with the expected common Trip IDs

    
    def test_get_stop_id(self):
        "Tests to see if stopId is the same as stopname"
        Stop_name = "Hlemmur B"
        expected_string = "90020295"
        result = self.bus.get_StopID(Stop_name)
        self.assertEqual(result, expected_string)

    def test_get_routeID(self):
        "Tests to see if tripID shows the correct routeID"
        tripID = "516175"
        expected_string = "AF.91"
        result = self.bus.get_routeID(tripID)
        self.assertEqual(result, expected_string)



    def test_get_bus_number(self):
        "tests to see if routeID gives the correct bus number"
        route_id = "NO.56"
        expected_string = "56"
        result = self.bus.get_bus_number(route_id)
        self.assertEqual(result, expected_string)

    def test_tripID(self):
        "tests to see if stopID returns the correct tripIDS"
        stop_id = ""
        excepted_list = []
        result = self.bus.get_true_tripID(stop_id)
        self.assertEqual(result, excepted_list)

    
    def test_print_favorite(self):
        "tests to see if printing works for favorite routes"
        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO)as mock_stdout:
            self.bus.print_favorite(["1"])
            self.assertEqual(
                mock_stdout.getvalue(),
                '1: Bus-1\n'
            )
    
    def test_find_arrivalTime(self):
        "tests to see if find_arrival_time returns the correct time"
        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO)as mock_stdout:
            trip_id = "509532"
            stop_id = "60000069"
            self.bus.find_arrival_time(trip_id, stop_id)
            self.assertEqual(
                mock_stdout.getvalue(),
                'Bus number: 1 Arrival time: 07:26:00\n'
            )

if __name__ == '__main__':
    unittest.main()
