import os
from concurrent import futures
import csv

class FavoriteBusStops:
    def __init__(self):
        pass
    
    '''def change_users_file(self):
        with open("Users.csv", "r") as file:
            reader = csv.reader(file)
            rows = list(reader)
            
            column_names = ["fbs-1", "fbs-2", "fbs-3", "fbs-4"]
            
            rows[0].extend(column_names)

            with open("Users.csv", "w", newline='') as file:
                writer = csv.writer(file)
                writer.writerows(rows)
               ''' 
    def add_favorite(self, email, favorite_stops):
        '''Adds favorite bus stops for a user identified by their email'''
        with open("Users.csv", "r") as file:
            reader = csv.reader(file)
            rows = list(reader)
            
        for row in rows:
            if str(email) == row[2]:  
                favorites = row[6:10] 
                count_non_empty = sum(1 for fav in favorites if fav.strip()) 
                for new_favorite in favorite_stops:
                    
                    if count_non_empty < 4:
               
                        for index, fav in enumerate(favorites):
                            if not fav.strip():
                                favorites[index] = new_favorite
                                count_non_empty += 1
                                break
                    else:
                        
                        break

                row[6:10] = favorites
                break
        
        with open("Users.csv", "w", newline='') as file:
            writer = csv.writer(file)
            writer.writerows(rows)
            
    def change_favorites(self,email, new_favorite_stops):
        '''Changes the favorite bus stops for a user identified by their email.'''
        with open("Users.csv", "r") as file:
            reader = csv.reader(file)
            rows = list(reader)
            
        for row in rows:
            if str(email) == row[2]:
                row[6:9] = new_favorite_stops
                break
            
        with open("Users.csv", "w", newline='') as file:
            writer = csv.writer(file)
            writer.writerows(rows)
            
    def remove_favorites(self, email, favorites_to_remove):
        ''' Removes favorite bus stops for a user identified by their email'''
        with open("Users.csv", "r") as file:
            reader = csv.reader(file)
            rows = list(reader)
        
        for row in rows:
            if str(email) == row[2]:
                for favorite in favorites_to_remove:
                    if favorite in row[1:]:
                        while favorite in row:
                            row.remove(favorite)
                        
                    break
                
                with open("Users.csv", "w", newline='') as file:
                    writer = csv.writer(file)
                    writer.writerows(rows)
                    
    def print_favorites(self, email, favorites_to_find):
        '''Print favorite bus stops for a user identified by their email'''
        with open("User.csv", "r") as file:
            reader = csv.reader(file)
            rows = list(reader)
            
        for row in rows:
            if str(email) == row[2]:
                for favorite in favorites_to_find:
                    if favorite in row[1:]:
                        column = row.index(favorite)
                        print(f"User {email} has '{favorite}' in {column}.")
                break
        
        
favorite_stops_instance = FavoriteBusStops()

   
#user_id = 2 

#favorites_to_find = ["Grandi"]

#new_favorite_stops = ["Grandi", "oddagata", "Klambratún"]
    
#favorite_stops_instance.change_favorites(user_id, new_favorite_stops)

#favorite_stops_instance.print_favorites(user_id, favorites_to_find)
