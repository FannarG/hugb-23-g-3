import os

class Weather:
    
    API_KEY = '4b7a2ac7-2a23-4e80-a574-c13f3d08f206'
    SERVICE_URL = 'hugb-service-ep3thvejnq-uc.a.run.app:443'
    
    def __init__(self, gtfs_folder):
        """Initialize the BusSchedule object by loading GTFS data."""
        self.stops = self.read_gtfs_stops(gtfs_folder)
      

    def get_user_input_bus_stop(self):
        """Getting user input"""
        bus_stop_name = input("Enter the bus stop name(capitalized first letter, example: Hlemmur): ")
        return bus_stop_name
    
    def read_gtfs_stops(self, gtfs_folder):
        """Read the 'stops.txt' file from GTFS data and return relevant data."""
        stops = []

        # Construct the path to the 'stops.txt' file
        stops_file = os.path.join(gtfs_folder, 'stops.txt')

        # Read the 'stops.txt' file and extract relevant information
        with open(stops_file, 'r', encoding='utf-8') as file:
            for line in file:
                stop_id, stop_name, stop_lat, stop_lon, location_type = map(str.strip, line.strip().split(','))
                stops.append((stop_name, stop_lat, stop_lon))

        return stops

    def get_stop_coordinates(self, bus_stop_name):
        '''Find the stop information based on the entered bus stop name'''
        for stop_name, stop_lat, stop_lon in self.stops:
            if stop_name == bus_stop_name:
                return stop_lat, stop_lon

        return None, None


    

