import os

def read_gtfs_file(filename):
    '''Read the content of a GTFS data file and return it as a list of lines.'''
    with open(filename, 'r', encoding='utf-8') as file:
        return file.readlines()


def parse_gtfs_data(data):
    '''Parse GTFS data by splitting lines into fields based on comma delimiter.'''
    parsed_data = [line.strip().split(',') for line in data]
    return parsed_data

def setup_gtfs_data(gtfs_folder):
    ''' Read and parse multiple GTFS data files from a given folder.'''
    calendar_dates_data = read_gtfs_file(os.path.join(gtfs_folder, 'calendar_dates.txt'))
    calendar_dates = parse_gtfs_data(calendar_dates_data)

    routes_data = read_gtfs_file(os.path.join(gtfs_folder, 'routes.txt'))
    routes = parse_gtfs_data(routes_data)

    trips_data = read_gtfs_file(os.path.join(gtfs_folder, 'trips.txt'))
    trips = parse_gtfs_data(trips_data)

    stop_times_data = read_gtfs_file(os.path.join(gtfs_folder, 'stop_times.txt'))
    stop_times = parse_gtfs_data(stop_times_data)

    stops_data = read_gtfs_file(os.path.join(gtfs_folder, 'stops.txt'))
    stops = parse_gtfs_data(stops_data)

   

    return calendar_dates, routes, trips, stop_times, stops
