import os
from . import gtfs_setup
from concurrent import futures


class SearchRoutes:
    """A class representing a bus schedule and related operations."""
    def __init__(self, gtfs_folder):
        """Initialize the BusSchedule object by loading GTFS data."""
        self.calendar_dates, self.routes, self.trips, self.stop_times, self.stops = self.setup_gtfs(gtfs_folder)
        self.numeric_bus_numbers = self.get_numeric_bus_numbers(self.routes)
        self.route_short_name_to_long_name = self.create_route_short_name_to_long_name_mapping(self.routes)

    def setup_gtfs(self, gtfs_folder):
        """Load GTFS data and return relevant datasets."""
        # Call the setup function to get GTFS data
        calendar_dates, routes, trips, stop_times, stops = gtfs_setup.setup_gtfs_data(gtfs_folder)
        return calendar_dates, routes, trips, stop_times, stops

    def create_route_short_name_to_long_name_mapping(self, routes):
        '''Create a mapping between numeric bus numbers (route_short_name) and route_long_name from routes.txt'''
        route_to_direction = {}
        for route in routes:
            route_short_name = route[2]
            route_long_name = route[3]
            if route_short_name.isdigit():
                if route_short_name not in route_to_direction:
                    route_to_direction[route_short_name] = set()
                route_to_direction[route_short_name].add(route_long_name)
        return route_to_direction
    
    def create_direction_set(self, routes):
        '''Create a set of unique directions based on route_long_name from routes.txt'''
        direction_set = set()
        for route in routes:
            route_long_name = route[3]
            direction_set.add(route_long_name)
        return direction_set

    def get_numeric_bus_numbers(self, routes):
        '''Extract bus numbers (route_short_name) from the 'routes.txt' file'''
        bus_numbers = set(route[2] for route in routes)
        numeric_bus_numbers = [bus_number for bus_number in bus_numbers if bus_number.isdigit()]
        sorted_numeric_bus_numbers = sorted([int(bus_number) for bus_number in numeric_bus_numbers])
        return sorted_numeric_bus_numbers

    def print(self):
        """Print available bus numbers with associated long names from routes.txt"""
        route_to_direction = self.create_route_short_name_to_long_name_mapping(self.routes)
        print()
        print("Below is a list of bus numbers and their directions. This list will allow you to more efficiently use the Bus Schedule located at number 1 in the Main Menu.")
        
        for bus_number in self.numeric_bus_numbers:
            directions = route_to_direction.get(str(bus_number), "")
            if directions:
                directions_str = ", ".join(directions)
                print(f"Bus number {bus_number}: {directions_str}")