#import gtfs_setup as gtfs_setup
from operator import itemgetter
from . import gtfs_setup
import csv



class Bus:

    def __init__(self, gtfs_folder):
        """Initialize the BusSchedule object by loading GTFS data."""
        self.calendar_dates, self.routes, self.trips, self.stop_times, self.stops = self.setup_gtfs(gtfs_folder)

    def setup_gtfs(self, gtfs_folder):
        """Load GTFS data and return relevant datasets."""

        # Call the setup function to get GTFS data
        calendar_dates, routes, trips, stop_times, stops = gtfs_setup.setup_gtfs_data(gtfs_folder)
        return calendar_dates, routes, trips, stop_times, stops


    
    def get_bus_times(self, start_location, stop_end, hour):
        """
        Retrieve and display bus times from a start location to a stop location at a specific hour.

        Parameters:
        - start_location (str): The starting location for the bus journey.
        - stop_end (str): The destination or stopping location for the bus journey.
        - hour (str): The hour for which bus times should be retrieved.
        """
        start_id = self.get_StopID(start_location)
        stop_id = self.get_StopID(stop_end)
        start_trip_ids = self.get_true_tripID(start_id)
        end_trip_ids = self.get_true_tripID(stop_id)
        true_trip_list = self.compare_tripID(start_trip_ids, end_trip_ids)
        trip_set = set()
        check_list = []
        for row in self.stop_times:
            s_stop_id = row[3]
            s_trip_id = row[0]
            arrival_time = row[1]
            if arrival_time.startswith(hour) and (s_stop_id== start_id) and (s_trip_id in true_trip_list):
                if arrival_time not in check_list:
                    trip_set.add((arrival_time, s_trip_id))
                    check_list.append(arrival_time)

        if not trip_set:
            return f"No buses go from {start_location} to {stop_end} "

        trip_set = sorted(trip_set, key=lambda x: x[0])
        print(f"All buses from {start_location} to {stop_end} at {hour}")
        counter = 0
        for item in trip_set:
            route_ID = self.get_routeID(item[1])
            bus_number = self.get_bus_number(route_ID)
            print(f"{counter}: {start_location} at {item[0]}, {bus_number}")
            counter += 1
        
        time = input("Choose what time you would like to take the bus: ")
        selected_trip = trip_set[int(time)]
        self.find_arrival_time(selected_trip[1],stop_id)    

    
    def available_by_date(self, input_date):
        """Search for bus routes by available dates.
            Arg:
                input_date(str): The specified date of choice.
            Returns:
                All buse routes that are traveling that date.

        """
        #Search for available buses by date
        bus_info = []
        for row in self.calendar_dates:
            service_id = row[0]
            dates_data = row[1]
            if input_date == dates_data:
                for x in self.trips:
                    if service_id == x[1]:
                        route_id = x[0]
                        for j in self.routes:
                            if route_id == j[0]:
                                if j[3] in bus_info:
                                    pass
                                else:
                                    bus_info.append(j[3])
        if not bus_info:
            return None
        else:
            print(bus_info)

            
    def get_StopID(self, stop):
        """
        Retrieve the Stop ID for a given location.

        Parameters:
        - location (str): The name of a location.

        Returns:
        - int: The Stop ID associated with the location.
        """
        for row in self.stops:
            stop_id = row[0]
            stop_name = row[1]
            if stop_name == stop:
                return stop_id



    def get_true_tripID (self, stop_ID):
        """
        Retrieve the true Trip IDs associated with a given Stop ID.

        Parameters:
        - stop_id (int): The Stop ID for which to find Trip IDs.

        Returns:
        - list: A list of true Trip IDs associated with the Stop ID.
        """
        trip_list = []
        for row in self.stop_times:
            trip_id = row[0]
            t_stop_id = row[3]
            if(t_stop_id == stop_ID):
                if (trip_id not in trip_list):
                    trip_list.append(trip_id)
        return trip_list
    
    def compare_tripID (self, start_stop_ID, end_stop_ID):
        """
        Compare lists of Trip IDs to find common elements.

        Parameters:
        - start_trip_ids (list): List of Trip IDs for the start location.
        - end_trip_ids (list): List of Trip IDs for the end location.

        Returns:
        - list: A list of Trip IDs that are common between start and end locations.
        """
        new_list = []
        for element in start_stop_ID:
            if element in end_stop_ID:
                new_list.append(element)
        return new_list
    
    def get_routeID(self, trip_ID):
        """
        Retrieve the Route ID associated with a Trip ID.

        Parameters:
        - trip_id (int): The Trip ID for which to find the Route ID.

        Returns:
        - int: The Route ID associated with the Trip ID.
        """
        for row in self.trips:
            route_id = row[0]
            r_trip_id = row[2]
            if trip_ID == r_trip_id:
                return route_id
    

    def get_bus_number(self, route_ID):
        """
        Retrieve the bus number associated with a Route ID.

        Parameters:
        - route_id (int): The Route ID for which to find the bus number.

        Returns:
        - str: The bus number associated with the Route ID.
        """
        for row in self.routes:
            r_route_id = row[0]
            bus_number = row[2]
            if r_route_id == route_ID:
                return bus_number
            
    def get_arrival_time(self, end_stop_ID, trip_ID):
        """
        Retrieve the arrival time of a specific trip at a given stop.

        Parameters:
        - end_stop_ID (int): The Stop ID where the arrival time is needed.
        - trip_ID (int): The Trip ID for which to find the arrival time.

        Returns:
        - str: The arrival time of the specified trip at the given stop.
        """
        for row in self.stop_times:
            s_stop_id = row[3]
            s_trip_id = row[0]
            arrival_time = row[1]
            if (s_stop_id== end_stop_ID) and (s_trip_id== trip_ID):
                return arrival_time
            
            
    
    def find_arrival_time(self, trip_ID, stop_id):
        """
        Find the arrival time of a specific trip at a given stop.

        Parameters:
        - trip_id (int): The Trip ID for which to find the arrival time.
        - stop_id (int): The Stop ID where the arrival time should be determined.

        Returns:
        - None
        """
        route_ID = self.get_routeID(trip_ID)
        bus_number = self.get_bus_number(route_ID)
        arrival_time = self. get_arrival_time(stop_id, trip_ID)
        print(f"Bus number: {bus_number} Arrival time: {arrival_time}")

    def print_favorite(self, favorites):
        """
        Prints a list of favorite bus routes.

        Parameters:
        - favorites(list): The list of your favorite bus routes

        Returns:
        - string of bus routes
        """
        for x in range(len(favorites)):
            print(f"{x+1}: Bus-{favorites[x]}")


