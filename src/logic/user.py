import grpc
import proto_pb2
import proto_pb2_grpc
from concurrent import futures
import csv
import sys

class Logic:
    """Class that encapsulates the business logic of application."""
    def __init__(self):
        """Initialize the Logic class with logged_in as False."""

        self.logged_in = False

    def create_user(self, request):
        """
        Create a new user based on the provided request.
        Returns:
            None
        """
        name = request.name
        email = request.email
        password = request.password
        security_question = request.security_question

        user_id = self.find_newest_ID("Users.csv") + 1
        with open("Users.csv", "a", newline="") as user_file:
            user_csv = csv.writer(user_file)
            user_csv.writerow([user_id, name, email, password, security_question])
        print("User created successfully.")


    def check_credentials(self, email, password):
        """
        Check if the provided email and password match a user's credentials.
        Args:
            email (str): The user's email.
            password (str): The user's password.
        Returns:
            bool: True if the credentials are valid, False otherwise.
        """
        # Load user data from "Users.csv" file
        with open("Users.csv", "r") as user_file:
            user_csv = csv.DictReader(user_file)

            for row in user_csv:
                if row["Email"] == email and row["Password"] == password:
                    return True  # Credentials match; user is authenticated

        return False  # No matching user found or incorrect password
    
    
    def login(self, email, password):
        """
        Perform user login.
        Returns:
            bool: True if login is successful, False otherwise.
        """

        # Check if the user exists and the password is correct
        if self.check_credentials(email, password):
            return True
        else:
            return False
        
    def logout(self):
        """Log the user out."""
        self.logged_in = False
        print("You have successfully logged out")
        return
    
    def initialize_file_system(self):
        """Initialize the file system by creating the 'Users.csv' file if it doesn't exist."""
        try:
            user_file = open("Users.csv", "r")
        except:
            user_file = open("Users.csv", "w", newline="")
            users_csv = csv.writer(user_file)
            users_csv.writerow(["User-ID","Name", "Email", "Password", "Security Question","fbs-1","fbs-2","fbs-3","fbs-4"])
            user_file.close()

    def find_newest_ID(self, file):
        """
        Find the highest User-ID in the given CSV file."""
    # Load user data from "Users.csv" file
        with open(file, "r") as user_file:
            user_csv = csv.DictReader(user_file)
            highest_id = 0

            for row in user_csv:
                user_id = int(row["User-ID"])
                highest_id = max(highest_id, user_id)

        return highest_id

    def update_email(self, email, new_email):
        """Update the email address for a user.
        Args:
            email (str): The user's current email.
            new_email (str): The new email address.
        Returns:
            bool: True if the email is updated successfully, False otherwise. """
       
        # Load user data from "Users.csv" file
        users = []
        with open("Users.csv", "r") as user_file:
            user_csv = csv.DictReader(user_file)
            for row in user_csv:
                if row["Email"] == email:
                    row["Email"] = new_email
                users.append(row)

        # Write updated data back to the CSV file
        with open("Users.csv", "w", newline="") as user_file:
            fieldnames = ["User-ID", "Name", "Email", "Password", "Security Question","fbs-1","fbs-2","fbs-3","fbs-4"]
            writer = csv.DictWriter(user_file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(users)

        return True  # Email updated successfully

    def update_name(self, email, new_name):
        """Update the name for a user.
        Args:
            email (str): The user's email.
            new_name (str): The new name to set.
        Returns:
            bool: True if the name is updated successfully, False otherwise."""
        # Load user data from "Users.csv" file
        users = []
        with open("Users.csv", "r") as user_file:
            user_csv = csv.DictReader(user_file)
            for row in user_csv:
                if row["Email"] == email:
                    row["Name"] = new_name
                users.append(row)

        # Write updated data back to the CSV file
        with open("Users.csv", "w", newline="") as user_file:
            fieldnames = ["User-ID", "Name", "Email", "Password", "Security Question","fbs-1","fbs-2","fbs-3","fbs-4"]
            writer = csv.DictWriter(user_file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(users)

        return True  # Name updated successfully


    def email_exists(self, email):
        """Check if an email address exists in the 'Users.csv' file.
        Args:
            email (str): The email address to check.
        Returns:
            bool: True if the email exists in the CSV file, False otherwise.
        """

        with open("Users.csv", "r", newline="") as file:
            csv_reader = csv.DictReader(file)
            for row in csv_reader:
                if row["Email"] == email:
                    return True  # Email exists in the CSV file
        return False  # Email does not exist in the CSV file

    def change_password(self, email, old_password, new_password):
        """
        Change the password for a user and update it in a CSV file.

        Args:
            email (str): The user's email.
            old_password (str): The user's current password.
            new_password (str): The new password to set.

        Returns:
            bool: True if the password was successfully updated, False otherwise.
        """
        # Load user data from "Users.csv" file
        users = []
        updated = False
        
        with open("Users.csv", "r") as user_file:
            user_csv = csv.DictReader(user_file)
            for row in user_csv:
                if row["Email"] == email:
                    if row["Password"] == old_password:
                        row["Password"] = new_password
                        updated = True
                users.append(row)

        # Write updated data back to the CSV file
        with open("Users.csv", "w", newline="") as user_file:
            fieldnames = ["User-ID", "Name", "Email", "Password", "Security Question", "fbs-1","fbs-2","fbs-3","fbs-4"]
            writer = csv.DictWriter(user_file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(users)

        return updated  # True if password updated successfully, False otherwise
    
    