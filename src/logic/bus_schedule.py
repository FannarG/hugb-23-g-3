import os
from . import gtfs_setup
from concurrent import futures


class BusSchedule:
    """A class representing a bus schedule and related operations."""
    def __init__(self, gtfs_folder):
        """Initialize the BusSchedule object by loading GTFS data."""
        self.calendar_dates, self.routes, self.trips, self.stop_times, self.stops = self.setup_gtfs(gtfs_folder)
        self.route_id_to_short_name = self.create_route_id_to_short_name_mapping(self.routes)
        self.bus_trip_headsigns = self.create_bus_trip_headsigns_dict(self.trips, self.route_id_to_short_name)
        self.numeric_bus_numbers = self.get_numeric_bus_numbers(self.routes)
    
    
    def setup_gtfs(self, gtfs_folder):
        """Load GTFS data and return relevant datasets."""

        # Call the setup function to get GTFS data
        calendar_dates, routes, trips, stop_times, stops = gtfs_setup.setup_gtfs_data(gtfs_folder)
        return calendar_dates, routes, trips, stop_times, stops


    def create_route_id_to_short_name_mapping(self, routes):
        '''Create a mapping between route_id and route_short_name from routes.txt'''
        route_id_to_short_name = {route[0]: route[2] for route in routes}
        return route_id_to_short_name

    def create_bus_trip_headsigns_dict(self, trips, route_id_to_short_name):
        '''Create a dictionary to store unique trip_headsigns for each bus number'''
        bus_trip_headsigns = {}

        for trip in trips:
            route_id = trip[0]
            trip_headsign = trip[3]
            route_short_name = route_id_to_short_name.get(route_id, "")

            if route_short_name not in bus_trip_headsigns:
                bus_trip_headsigns[route_short_name] = set()

            bus_trip_headsigns[route_short_name].add(trip_headsign)

        return bus_trip_headsigns

    def get_numeric_bus_numbers(self, routes):
        '''Extract bus numbers (route_short_name) from the 'routes.txt' file'''
        bus_numbers = set(route[2] for route in routes)
        numeric_bus_numbers = [bus_number for bus_number in bus_numbers if bus_number.isdigit()]
        sorted_numeric_bus_numbers = sorted([int(bus_number) for bus_number in numeric_bus_numbers])
        return sorted_numeric_bus_numbers

    def print_available_bus_numbers(self):
        '''Print available bus numbers with commas and split into rows'''
        print()
        print("Available Bus Numbers:")
        for i, bus_number in enumerate(self.numeric_bus_numbers, start=1):
            if i % 10 == 0:
                print(bus_number)
            else:
                print(bus_number, end=", ")
      

    def get_selected_route(self):
        '''Prompt the user to enter a valid bus number and direction'''
        print()
        print()
        selected_route = input("Enter the bus number: ").strip()
        return selected_route

    def validate_selected_route(self, selected_route):
        '''Check if the entered route is valid'''
        if selected_route not in map(str, self.numeric_bus_numbers):
            print("Invalid bus number. Please enter a valid bus number.")
            return False
        return True

    def get_selected_direction(self, route_short_name):
        """Prompt the user to choose a direction for the bus."""
        unique_trip_headsigns = self.bus_trip_headsigns.get(route_short_name, set())
        unique_trip_headsigns.discard('')
        if len(unique_trip_headsigns) > 1:
            print(f"Directions for Bus {route_short_name}:")
            for i, trip_headsign in enumerate(unique_trip_headsigns, start=0):
                print(f"{i}: {trip_headsign}")
            selected_direction = input(f"Choose a direction for Bus {route_short_name} (0 or 1): ")
            if selected_direction not in ['0', '1']:
                print("Invalid direction choice. Please enter 0 or 1.")
                return None
            return selected_direction
        return None

    def get_selected_day(self):
        """Prompt the user to enter a specific date in 'yyyymmdd' format."""
        selected_day = input("Enter a date (yyyymmdd): ")
        return selected_day


    '''def get_service_id_for_date(self, selected_day):
        """Check 'calendar_dates.txt' for a matching date and return the associated 'service_id'."""
   
        if selected_day:
            service_id = None

            # Iterate through 'calendar_dates' data
            for row in self.calendar_dates:
                date_in_file = row[1]
                if date_in_file == selected_day:
                    service_id = row[0]
                    break

            if service_id:
                return service_id
            else:
                print("No service found for the specified date.")
                return None
        else:
            return None'''

    def filter_selected_trips(self, selected_route, selected_direction, selected_day):
        """Filter trips based on bus number (route_short_name), direction, and service day."""


         # Get the service_id for the selected date
        #service_id = self.get_service_id_for_date(selected_day)

        selected_trips = [
            trip for trip in self.trips
            if (
                self.route_id_to_short_name.get(trip[0]) == selected_route
                and trip[5] == selected_direction
                   
            )
        ]


        return selected_trips
    

    def create_stop_schedule(self, selected_trips):
        '''Create a dictionary to store stop information and arrival times'''
        stop_schedule = {}

        for trip in selected_trips:
            trip_id = trip[2]
            # Find stop times for the selected trip_id
            trip_stop_times = [stop_time for stop_time in self.stop_times if stop_time[0] == trip_id]

            for stop_time in trip_stop_times:
                stop_id = stop_time[3]
                arrival_time = stop_time[1]

                # Check if stop_id already exists in the schedule dictionary
                if stop_id not in stop_schedule:
                    stop_schedule[stop_id] = []

                # Append arrival time to the stop's schedule
                stop_schedule[stop_id].append(arrival_time)

        return stop_schedule
    
    def get_stops(self):
        """Return the stops data."""
        return self.stops
    
    def print_schedule(self, selected_route, selected_direction, selected_day, stop_schedule, stops):
        '''Print the consolidated schedule for the selected route, direction, and day'''
        print(f"Schedule for Bus {selected_route}, Direction {selected_direction}, {selected_day}:")
        print()
        for stop_id, arrival_times in stop_schedule.items():
            # Find the stop name based on the stop_id
            stop_info = next((stop for stop in stops if stop[0] == stop_id), None)
            if stop_info:
                stop_name = stop_info[1]
                # Sort the arrival times in chronological order
                sorted_arrival_times = sorted(arrival_times)
                formatted_arrival_times = [time[:5] for time in sorted_arrival_times]  # Extract only hours and minutes
                arrival_times_str = ", ".join(formatted_arrival_times)
                print(f"Stop {stop_name}: {arrival_times_str}")
            print()  # Add an empty line after each stop
    
