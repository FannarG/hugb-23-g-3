import grpc
import proto_pb2_grpc
import proto_pb2
import weatherStation_pb2_grpc
import weatherStation_pb2
import sys
import logic.user as user 
from logic.bus_schedule import BusSchedule
from logic.weather import Weather
from logic.bus import Bus
from logic.search_routes import SearchRoutes
from logic.favorites_bus_stops import FavoriteBusStops




# Global variable to track login status
logged_in = False
favorites = []


def create_user():
    """Creates a new user.
    Returns:
        information about the user in the database 
    """
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = proto_pb2_grpc.AppStub(channel)
        
        # Prompt the user for input
        name = input("Enter your name: ")
        email = input("Enter your email: ")
        password = input("Enter your password: ")
        security_question = input("What was the first street you lived on called?: ")
        logic_instance = user.Logic()

        #Test to see if email is unique
        if logic_instance.email_exists(email):
            print(f"The email '{email}' already exists. User creation failed.")
            # Exit the function if email is not unique
            return main_menu()  
        
        request = proto_pb2.CreateUserRequest(
            name=name,
            email=email,
            password=password,
            security_question=security_question
        )
        response = stub.CreateUser(request)
        
        # Process the response
        print(response.message)


def login(email, password):
    """ Login for existing user.
    Returns:
        boolen: True if email and password exits in csv file, False otherwise
    """


    with grpc.insecure_channel("localhost:50051") as channel:
        stub = proto_pb2_grpc.AppStub(channel)
        request = proto_pb2.LoginRequest(email=email, password=password)
        response = stub.Login(request)
        return response  # Return the response object


def change_password(email, old_password, new_password):
    """ Function to change password"""
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = proto_pb2_grpc.AppStub(channel)
        request = proto_pb2.ChangePasswordRequest(
            email=email,
            old_password=old_password,
            new_password=new_password
        )
        response = stub.ChangePassword(request)
        if response.success:
            print("Password changed successfully")  # Ensure this message is printed
        else:
            print("Password change failed")


def main_menu():
    """ Runs the main menu until the user clicks quit."""
    while True:
        print("\nMenu:")
        print("1. Register")
        print("2. Login")
        print("3. Quit")
        
        choice = input("Enter your choice: ")

        if choice == "1":
            create_user()
        elif choice == "2":
            global logged_in
            email = input("Enter your email: ")
            password = input("Enter your password: ")

            if not email or not password:
                print("Both email and password are required.")
            else:
                response = login(email, password)

                if response.success:
                    logged_in = True
                    print("Login successful.")
                    user_menu(email)  # Proceed to the user menu
                else:
                    print("Login failed. Reason:", response.message)
                    reset = input("Would you like to reset your password? y/n: ")
                    if reset == "y":
                        security = input("What was the first street you lived on called?: ")
                        new_pas = input("What do you want your new password to be?: ")
                        reset_password(email, security, new_pas)

        elif choice == "3":
            print("Goodbye!")
            sys.exit()
        else:
            print("Invalid choice. Please try again.")


# Define the user menu function
def user_menu(email):
    """Prints the user menu for the given email.
    Args:
        email (str): email address
    """
    global logged_in
    while True:
        print("\nLogged-in User Menu:")
        print("1. Bus Schedule")
        print("2. Search for a bus")
        print("3. Manage favourite stops and routes")
        print("4. Edit Profile Information")
        print("5. View weather for bus station")
        print("6. Logout")
        inner_choice = input("Enter your choice: ")
        if inner_choice == "1":
            gtfs_folder = 'gtfs/'
            bus_schedule = BusSchedule(gtfs_folder)
            # Print available bus numbers
            bus_schedule.print_available_bus_numbers()
            selected_route = bus_schedule.get_selected_route()
            selected_direction = bus_schedule.get_selected_direction(selected_route)
            selected_day = bus_schedule.get_selected_day()

            # Filter selected trips based on user input
            selected_trips = bus_schedule.filter_selected_trips(selected_route, selected_direction, selected_day)
            # Create a stop schedule based on selected trips
            stop_schedule = bus_schedule.create_stop_schedule(selected_trips)
            stops = bus_schedule.get_stops()
            # Print the schedule
            #bus_schedule.print_schedule(selected_route, selected_direction, selected_day, stop_schedule, bus_schedule.stops)
    
            bus_schedule.print_schedule(selected_route, selected_direction, selected_day, stop_schedule, stops)

        elif inner_choice == "2":
            bus_menu()
        elif inner_choice == "3":
            manage_favorites(email)
        elif inner_choice == "4":
            edit_profile_menu(email)
        elif inner_choice == "5":
            #bus_stop_name = get_user_input_bus_stop()
            #weather_info = get_weather_info(bus_stop_name)
            #show_weather_results(weather_info)
            check_weather()
        elif inner_choice == "6":
            print("You are logged out")
            logged_in = False
            break
        else:
            print("Invalid choice. Please try again.")

def bus_menu():
    """ Runs the bus menu until the user clicks quit."""
    while True:
        print("\nBus Menu:")
        print("1. Search by time")
        print("2. Search by date")
        print("3. Search by route")
        print("4. Go Back")
            
        choice = input("Enter your choice: ")

        if choice == "1":
            gtfs_folder = 'gtfs/'
            bus = Bus(gtfs_folder)
            time = input("Enter what hour you would like to travel (example 08): ")
            print("Bus stops need to be on the same route for search to work")
            startstop = input("Enter starting location (Example Hlemmur B): ")
            endstop = input("Enter destination (Example Egilshöll B): ")
            bus.get_bus_times(startstop, endstop, time)

        elif choice == "2":
            gtfs_folder = 'gtfs/'
            bus = Bus(gtfs_folder)
            date = input("Enter the date you want to travel on (example: yyyymmdd(20231010)): ")
            bus.available_by_date(date)
        elif choice == "3":
            gtfs_folder = 'gtfs/'
            bus = Bus(gtfs_folder)
            Bus.print_favorite(favorites)
            search_routes = SearchRoutes(gtfs_folder)
            search_routes.print()
            pass
        elif choice == "4":
            break
        else:
            print("Invalid choice. Please try again.")

def add_favorite_bus_stops(email, new_favorite_stops):
    """sends a request to the server to add a new favorite stop

    Args:
        email (_str_): _description_
        new_favorite_stops (list_of_str): _description_

    Returns:
        boolean: succes = True
    """
    channel = grpc.insecure_channel('localhost:50051')
    stub = proto_pb2_grpc.AppStub(channel)
    
    request = proto_pb2.AddFavoriteRequest(email=email, new_favorite_stops=new_favorite_stops)
    response = stub.AddFavoriteBusStops(request)
    return response

def remove_favorite_bus_stops(email, favorites_to_remove):
    """sends a request to the server to remove a new favorite bus stop

    Args:
        email (_str_): _description_
        favorites_to_remove (list_to_str): _description_

    Returns:
        boolean: succes = True
    """
    channel = grpc.insecure_channel('localhost:50051')
    stub = proto_pb2_grpc.AppStub(channel)
    
    request = proto_pb2.RemoveFavoriteRequest(email=email, favorites_to_remove=favorites_to_remove)
    response = stub.RemoveFavoriteBusStops(request)
    return response

def change_favorite_bus_stops(email, new_favorites):
    """sends a request to the server to change a new favorite bus stop

    Args:
        email (_str_): _description_
        new_favorites (list_of_str): _description_

    Returns:
        boolean: succes = True
    """
    channel = grpc.insecure_channel('localhost:50051')
    stub = proto_pb2_grpc.AppStub(channel)
    
    request = proto_pb2.ChangeFavoriteBusStopRequest(email=email, new_favorites = new_favorites)
    response = stub.ChangeFavoriteBusStops(request)
    return response
         
def manage_favorites(email):
    """ Runs the manage favorite menu until the user clicks quit."""
    while True:
        print("\nManage favorite menu:")
        print("1. Add Favorite Bus Stops")
        print("2. Remove Favorite Bus Stops")
        print("3. Change Favorite Bus Stops")
        print("4. Print favorite Bus Routes")
        print("5. Add favorite Bus Route")
        print("6. Remove favorite Bus Route")
        print("7. Go back")
        
        inner_choice = input("Enter your choice: ")
        if inner_choice == "1":
            new_favorite_stops = input("Enter new favorite bus stops (comma-separated): ").split(',')
            add_favorite_bus_stops(email, new_favorite_stops)
            print("Favorite bus stops added successfully")
        
        elif inner_choice == "2":
            user_id = email
            favorites_to_remove = input("Enter favorites bus stops to remove (comma-separated): ").split(',')
            remove_favorite_bus_stops(user_id, favorites_to_remove)
            print("Favorite bus stops removed successfully")
            
        elif inner_choice == "3":
            new_favorites = input("Enter favorites bus stops (comma-separated): ").split(',')
            change_favorite_bus_stops(email, new_favorites)
            print("Favorite bus stops changed successfully")
        
        elif inner_choice == "4":
              gtfs_folder = 'gtfs/'
              bus = Bus(gtfs_folder)
              bus.print_favorite(favorites)
        
        elif inner_choice == "5":
            gtfs_folder = 'gtfs/'
            bus_schedule = BusSchedule(gtfs_folder)
            bus = Bus(gtfs_folder)
            bus_schedule.print_available_bus_numbers()
            selected_route = bus_schedule.get_selected_route()
            if selected_route not in favorites:
                favorites.append(selected_route)
                print("Bus number " + selected_route + " is now one of your favorite route")
                bus.print_favorite(favorites)
            else:
                print("Bus number " + selected_route + " is already one of your favorite route")
        elif inner_choice == "6":
            gtfs_folder = 'gtfs/'
            bus_schedule = BusSchedule(gtfs_folder)
            bus = Bus(gtfs_folder)
            bus.print_favorite(favorites)
            digit = False
            while digit == False:
                remove = input("What bus do you want to remove: ")
                if remove.isdigit():    
                    remove =- 1
                    del favorites[remove]
                    digit = True
                else:
                    print("Please input the number of the bus route you wish to remove")    
        elif inner_choice == "7":
            break
        else:
            print("Invalid choice. Please try again.")

def edit_profile_menu(email):
    """Menu that edits profile based on email.
    Args:
        email (str): email address
    """
    while True:
        print("\nEdit Profile Information:")
        print("1. Change Password")
        print("2. Change Name")
        print("3. Change Email")
        print("4. Go Back")
        inner_choice = input("Enter your choice: ")
        if inner_choice == "1":
            old_password = input("Enter your old password: ")
            new_password = input("Enter your new password: ")
            change_password(email, old_password, new_password)
        elif inner_choice == "2":
            new_name = input("Enter your new name: ")
            change_name(email, new_name)
        elif inner_choice == "3":
            new_email = input("Enter your new email: ")
            change_email(email, new_email)
        elif inner_choice == "4": 
            break
        else:
            print("Invalid choice. Please try again.")

def change_password(email, old_password, new_password):
    """
    Change the password for a user.
    Args:
        email (str): The user's email.
        old_password (str): The user's current password.
        new_password (str): The new password to set.
    Returns:
        bool: True if the password change was successful, False otherwise.
    """
    # Check if the old password is correct
    if not login(email, old_password):
        print("Incorrect old password. Try again.")
        return False  # Return False to indicate the password change was not successful
    # Check if the new password is the same as the old one
    if old_password == new_password:
        print("New password is the same as the old password. Password change failed.")
        return False

    with grpc.insecure_channel("localhost:50051") as channel:
        stub = proto_pb2_grpc.AppStub(channel)
        request = proto_pb2.ChangePasswordRequest(
            email=email,
            old_password=old_password,
            new_password=new_password
        )
        try:
            response = stub.ChangePassword(request)  # Note the method name
            if response.success:
                print("Password changed successfully.")
                return True
            else:
                print("Failed to change password:", response.message)
                return False
        except grpc.RpcError as e:
            print("Error during gRPC call:", e)
            return False

def reset_password(email, security_quest, new_password):
    """Reset the password in case user forgot"""

    with grpc.insecure_channel("localhost:50051") as channel:
        stub = proto_pb2_grpc.AppStub(channel)
        request = proto_pb2.ResetPasswordRequest(
            email=email,
            security_quest=security_quest,
            new_password=new_password
        )
        try:
            response = stub.ResetPassword(request)  # Note the method name
            if response.success:
                print("Password changed successfully.")
                return True
            else:
                print("Failed to change password:", response.message)
                return False
        except grpc.RpcError as e:
            print("Error during gRPC call:", e)
            return False



def change_name(email, new_name):
    """Changes name.
    Args:
        email (str): Email
        new_name (str): Name to be changed
    Returns:
        boolean: True if the name was changed successfully, False otherwise
    """
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = proto_pb2_grpc.AppStub(channel)
        request = proto_pb2.Change_NameRequest(
            email=email,
            new_name=new_name
        )
        try:
            response = stub.Change_Name(request)
            if response.success:
                print("Name changed successfully.")
                return True
            else:
                print("Failed to change Name:", response.message)
                return False
        except grpc.RpcError as e:
            print("Error during gRPC call:", e)
            return False

def change_email(email, new_email):
    """Changes the email address.
    Args:
        email (str): email address to change
        new_email (str): new email 

    Returns:
        boolean: True if it is succsesfull, otherwise False
    """
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = proto_pb2_grpc.AppStub(channel)
        request = proto_pb2.Change_EmailRequest(
            email=email,
            new_email=new_email
        )
        try:
            response = stub.Change_Email(request)
            if response.success:
                print("Email changed successfully.")
                return True
            else:
                print("Failed to change Email:", response.message)
                return False
        except grpc.RpcError as e:
            print("Error during gRPC call:", e)
            return False
        
def view_bus_schedule():
    """Function to view the bus schedule."""
    
     # Initialize BusSchedule with the path to your GTFS data
    gtfs_folder = 'gtfs/'
    bus_schedule = BusSchedule(gtfs_folder)
     # Print available bus numbers
    bus_schedule.print_available_bus_numbers()
    selected_route = bus_schedule.get_selected_route()
    selected_direction = bus_schedule.get_selected_direction(selected_route)
    selected_day = bus_schedule.get_selected_day()
    

    # Create a gRPC channel to connect to the server (replace with your server address)
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = proto_pb2_grpc.AppStub(channel)
        request = proto_pb2.BusScheduleRequest(
            selected_route=selected_route,
            selected_direction=selected_direction,
            selected_day=selected_day
        )
        response = stub.GetBusSchedule(request)
        return response

def arrival_time(selected_start, selected_end, selected_time):
    """Function to display bus number if user search bus by time"""
    gtfs_folder = 'gtfs/'
    bus = Bus(gtfs_folder)

    bus.get_bus_times(selected_end, selected_start, selected_time)

    with grpc.insecure_channel("localhost:50051") as channel:
        stub = proto_pb2_grpc.AppStub(channel)
        request = proto_pb2.ViewArrivalRequest(
            selected_start=selected_start,
            selected_end=selected_end,
            selected_time=selected_time
        )
        response = stub.GetBusSchedule(request)
        return response



def check_weather():
    '''This function display weather for selected bus stop'''
    API_KEY = '4b7a2ac7-2a23-4e80-a574-c13f3d08f206'
    SERVICE_URL = 'hugb-service-ep3thvejnq-uc.a.run.app:443'
    gtfs_folder = './gtfs'
    weather = Weather(gtfs_folder)

    # Get the bus stop name from the user
    bus_stop_name = weather.get_user_input_bus_stop()

    # Get the latitude and longitude of the bus stop
    latitude, longitude = weather.get_stop_coordinates(bus_stop_name)
    latitude = float(latitude)  # Convert latitude to float
    longitude = float(longitude)
    if latitude is not None and longitude is not None:
        try:
            #Here is a difference to the client we have used so far: We are using a secure channel.
            with grpc.secure_channel(SERVICE_URL, grpc.ssl_channel_credentials()) as channel:

                #This is just as before: We import the grpc definitions.
                stub = weatherStation_pb2_grpc.WeatherStationStub(channel)
                current_weather = stub.get_current_weather(weatherStation_pb2.Location(lat=latitude, lon=longitude, api_key=API_KEY))
                #Call the three different methods. For parameter and return type descritions, see the proto file
                print("Current weather: " , current_weather)
               
                

        #Error handling in case the grpc connection throws an error. This is how the service provides errors.
        except grpc.RpcError as e:
            # print gRPC error message
            print(e.details())
            status_code = e.code()
            # should print `INVALID_ARGUMENT`
            print(status_code.name)
            # should print `(3, 'invalid argument')`
            print(status_code.value)
    else:
        #If no error, print the return values of the three different calls.
        print("Bus stop not found.")
      


if __name__ == "__main__":
    main_menu()
