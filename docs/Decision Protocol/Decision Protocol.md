The decision protocol serves as a concise record of all significant decisions made by our group during the course of the project. This document captures important choices, rationale, and dates, allowing us to maintain a clear understanding of our project's direction and key determinations. The protocol will be periodically updated as new decisions are reached, providing a valuable resource for team members and teaching assistants to quickly grasp the project's progress and decision-making process.


**Sprint 1**

TA Meeting (29/08/2023)
- Presented our user-manuals to each other.
- Clap-IT chosen as product domain through majority vote.
- Majority vote used to resolve most decisions.
- Git-lab repo created.
- Discord channel was created for better communication.
- Team members, TAs and instructors added to the repo.
- User stories were divided between team members.
- Project overview report started.


Monday Meeting (04/09/2023)
- Requirement list created, requirements ranked from A to C.
- Backlog created and 13 user stories as a Issues added to backlog.
- All user stories follow the format: 'A a user, I want [feature], so that...'


TA Meeting (05/09/2023)
- Moved Clap-IT from being a all encompassing bus app to a simpler busroute mananging system.
- Clap-IT will not handle payment in any way.
- 3 scenarios were added to report, all of them based on user stories from backlog.


Thursday Meeting (08/09/2023)
- Decided to keep the project simple.
- Project shifted to focus more on tracking, liking and creating favorite routes.
- Decided to create three diagrams for the project overview report(Class Diagram, Sequence Diagram, Use Cases Diagram).


Monday Meeting (11/09/2023)
- Changed decision protocol to cotain more high-level points.
- Decison protocol relocated to folder with the same name, under docs directory.
- Added 2 Personas and 3 diagrams to report.
- Finished overview report.
- Overview report added as a pdf file to Sprint1 folder.

**Sprint 2**

TA Meeting (12/09/2023)
- Fannar chosen randomly as the Scrum Master
- 4 user stories were selected for the implementation during Sprint 2 (user stories: 1, 2, 3 and 12)
- A table with estimated implementation time for all useer stories has been added to the Sprint 2 folder


Wednesday Meeting (13/09/2023)
- The definition of DoD was established and has been added to the Sprint 2 folder
- User stories have been broken down into tasks and divide among team members

TA Meeting (19/09/2023)
- Fixed wording of Definition of Done
- Class diagram turned into non technical domain model
- Use case diagram and Sequence diagram removed from overview-report
- Report for Sprint1 updaded

Thursday (21/09/2023)
- gRPC API used for coding
- unittest used for unit tests

Monday Meeting (25/09/2023)
- Completing the code for Sprint 2
- Docstrings have been added to the code
- Read.me file added to root directory with instructions on how to run application


**Sprint 3**

TA Meeting (26/09/2023)
- Theodor chosen randomly as the Scrum Master
- 4 user stories were selected for the implementation during Sprint 3: (user stories: 4, 5, 6 and 7)
- Estimated implementation time for above 4 user stories has been added to them.

Monday Meeting (02/10/2023)
- The primary data source for this project is the GTFS feed provided by Straeto
- The definition of DoD was established and has been added as a Issue

TA Meeting (03/10/2023)
- gRPC files have been processed and read, now we are working on our code


TA Meeting (09/10/2023)
- Docstrings have been added to the code
- Completing the code for Sprint 3
- 5 code reviews has been added to the Backlog of group 7
 


**Sprint 4**

TA Meeting (10/10/2023)
- Ari chosen randomly as the Scrum Master
- We are going to countinue implement user stories: 4,5,6 and 7

Thursday Meeting (12/10/2023)
- The definition of DoD was established and has been added as a Issue
- We have choosen get_current_weather API function to show the weather in a given location when users search bus

Monday Meeting (16/10/2023)
- We took note of the review by group 21, member Matthias Sylviuson, and therefore fixed the file tests_users.py to make unit tests work independently without interfering with the user.csv file 
- A new command has been created to display tests for entire folder with tests instead of display single report for each uniitest file: 'coverage run -m unittest discover -s tests'

Monday Meeting (23/10/2023)
- All mising Docstrings have been added to the code
- More tests were created to cover 50% of the code



**Sprint 5**

TA Meeting (24/10/2023)
- Anna was chosen randomly as the Scrum Master
- We are going to implement user stories: 8,9,10 and 11
- In this sprint we need to finish implementation of Weather API
- The DoD was established and added as an Issue

TA Meeting (31/10/2023)
- Fixed a protobuf and protoc issues
- From now on we going to use protobuf==3.20.0
- Created tests for weather API

Wednesday Meeting (01/11/2023)
- Weather API was finished
- Set up continuous integration (CI) with pipeline and coverage badges

Friday Meeting (03/11/203)
- All mising Docstrings have been added to the code
- More tests were created to cover 50% of the code
